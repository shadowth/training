const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    username: {
        type: String,
        unique: true
    },
    email: String,
    password: String,
    // One-to-Many Relationship tutorial with Mongoose 
    // Reference Data Models or Normalization
    roles: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Role"
        }
    ]
});

const User = mongoose.model('User', userSchema);

export default User;