const express = require('express');
const router = express.Router();
import authJwt from '../middlewares/authJwt';

const user = require('../controllers/user.controller');

router.get('/', user.getAllUsers);
router.post('/', user.createUser);
router.delete('/deleteUser/:firstname', user.deleteUser);
router.patch('/updateUser/:id', user.updateUser);
router.get('/getUser/:fname', user.getUserByName);
router.get('/getNumberOfUsers', user.getNumberOfUsers);
// router.get('/user', user.userBoard);
// router.get(
//     "/mod",
//     [authJwt.verifyToken, authJwt.isModerator],
//     user.moderatorBoard
// );
// router.get(
//     "/admin",
//     [authJwt.verifyToken, authJwt.isAdmin],
//     user.adminBoard
// );

module.exports = router;