import verifySignUp from '../middlewares/verifySignUp';
const controller = require("../controllers/auth.controllers");

// module.exports = function(app) {
//     app.use(function(req, res, next) {
//       res.header(
//         "Access-Control-Allow-Headers",
//         "x-access-token, Origin, Content-Type, Accept"
//       );
//       next();
//     });
//     app.post(
//       "auth/signup",
//       controller.signup
//     );
//     app.post("auth/signin", controller.signin);
// };

const express = require('express');
const router = express.Router();

// router.get('/', user.getAllUsers);
router.use((req, res, next) => {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});
// router.use()
router.post(
  '/signup',
  [verifySignUp.checkDuplicateUsernameOrEmail, verifySignUp.checkRolesExisted],
  controller.signup
);
router.post('/signin', controller.signin);


module.exports = router;