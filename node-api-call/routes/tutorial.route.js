const express = require('express');
const router = express.Router();

const tutorial = require('../controllers/tutorial.controller');
import authJwt from '../middlewares/authJwt';
const user = require('../controllers/user.controller');

router.get('/', tutorial.getAllTuts);
router.post('/', tutorial.createTut);
router.delete(
    '/deleteTut/:title', 
    [authJwt.verifyToken, authJwt.isModerator],
    tutorial.deleteTut
);
router.put('/updateTut/:id', [authJwt.verifyToken], tutorial.updateTut);
router.get('/detail/:title', tutorial.getTutorialByTitle);
router.get('/search', tutorial.getSearchResult);
router.get('/page', tutorial.getPage);
router.get(
    '/tutor-course/:tutor', 
    [authJwt.verifyToken, authJwt.isModerator],
    tutorial.getTutorialsByTutor
);
router.get(
    '/subscriber-course/:subscriber', 
    [authJwt.verifyToken, authJwt.isUser],
    tutorial.getTutorialsBySubscriber
);
router.get(
    '/admin/tutorialData', 
    [authJwt.verifyToken, authJwt.isAdmin],
    tutorial.getTutorialData
);
router.get(
    '/admin/userData',
    [authJwt.verifyToken, authJwt.isAdmin],
    user.getUserData
)
router.get(
    '/admin/tutorialPastDays/:days',
    tutorial.getTutorialPastDays
)

module.exports = router;