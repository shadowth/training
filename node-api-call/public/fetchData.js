fetch('http://localhost:8080/')
    .then( response => {
        // const users = response.json().then(data => data);
        // console.log(users)
        return response.json();
    })
    .then(data => {
        // console.log(data);
        // appendData(data);
        showData(data);
    })
    .catch(function (err) {
        console.log('error: ' + err);
    });

function appendData(data) {
    // Need to use table element for insertRow and insertCell
    console.log(data)
    let table = document.getElementById('data');
    
    for (let i = 0; i < data.length; i++) {
        var row = table.insertRow(-1);

        // Object to array
        var array = Object.values(data[i]);
        console.log(data[0])

        for (var j=1; j < 4; j++) {
            var cell = row.insertCell(-1);
            cell.innerHTML = array[j];
        }    
    }
}

function showData(data) {
    for (let i = 1; i < 6; i++) {
        let id = 'card-id-' + i;
        let card = document.getElementById(id);
        card.innerHTML = '<div class="card-body">' + 
            '<h5 class="card-title" onclick="enablePopup()">' + data[i].username + '</h5>' +
                '<p class="card-text">' + data[i].firstname + ' ' + data[i].lastname + '</p>' +
                '<p class="card-text"><small class="text-muted">Last updated in ' + data[i].city +'</small></p>' +
            '</div>';
    }
}

function addSticky() {
    var header = document.getElementById("navbar--effect");
    if (window.pageYOffset > 300) {
        header.classList.remove("navbar-dark");
        header.classList.add("navbar-light");
        header.classList.add("bg-light");
    }
    else {
        header.classList.add("navbar-dark");
        header.classList.remove("navbar-light");
        header.classList.remove("bg-light");
    }
}

function replaceDiv() {
    let first = document.getElementById()
}

function enablePopup () {
    document.querySelector(".popup-view").style.display = "flex";
}

function disablePopup () {
    document.querySelector(".popup-view").style.display = "none";
}

