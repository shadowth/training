const config = require("../config/auth.config");
import User from '../models/user.model';
import Role from '../models/role.model';
let jwt = require("jsonwebtoken");
let bcrypt = require("bcryptjs");

export const signup = (req, res) => {
  const role = req.body.role == "subscriber" ? "user" : "moderator" ;
  const user = new User({
      username: req.body.username,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
  });
  if (role != "user" && role != "moderator") {
    res.status(500).send({ message: "No role provided." })
    return;
  }
  user.save((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      if (role) {
        Role.findOne({ name: role }, (err, role) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }
          user.roles = [role._id];
          user.save(err => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }
            res.send({ message: "User was registered successfully!" });
          });
        });
      }
  });
};

export const signin = (req, res) => {
  User.findOne({
    username: req.body.username
  })
    .populate("roles", "-__v")
    .exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }
      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );
      if (!passwordIsValid) {
        return res.status(401).send({
            accessToken: null,
            message: "Invalid Password!"
        });
      }
      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400 // seconds 
      });
      let authorities = [];
      for (let i = 0; i < user.roles.length; i++) {
        authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
      }
      res.status(200).send({
          id: user._id,
          username: user.username,
          email: user.email,
          roles: authorities,
          accessToken: token
      });
  });
};