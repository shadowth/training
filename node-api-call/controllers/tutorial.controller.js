import Tutorial from '../models/tutorial.model';

export const getAllTuts = async (req, res) => {
    try {
        const tuts = await Tutorial.find();
        res.status(200).send(tuts);
    } catch (error) {
        res.status(404).send({ message: error.message });
    }
}

export const createTut = async (req, res) => {
    const tut = new Tutorial(req.body);

    try {
        console.log(tut);
        await tut.save();
        res.status(201).json(tut);

    } catch (error) {
        res.status(409).send({ message: error.message });
    }
}

export const updateTut = async (req, res) => {
    const id = req.params.id;
    const tut = req.body;

    try {
        const prev = await Tutorial.findById(id);
        if (!prev) {
            res.status(404).send('Tutorial is not existed!');
        }
        else {
            await Tutorial.findByIdAndUpdate(id, tut);
            res.status(200).send({ message: 'Updated successfully!' });
        }
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
}

export const deleteTut = async (req, res) => {
    const title = req.params.title
    console.log('delete: ' + title);
    try {
        await Tutorial.findOneAndDelete({ title: title });
        res.send({ message: 'Deleted successfully!' });
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
}

export const getTutorialByTitle = async (req, res) => {
    const title = req.params.title;
    // console.log(title);

    try {
        const tutorial = await Tutorial.findOne({ "title": title });
        if (tutorial) {
            res.status(202).send(tutorial);
        }
        else {
            res.status(400).send('Tutorial is not existed!');
        }
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
}

export const getSearchResult = async (req, res) => {
    const key = req.query.key;

    try {
        const result = await Tutorial.find({ title: { $regex: key, $options: 'i' } });
        res.status(200).send(result);
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
   
}

export const getPage = async (req, res) => {
    const page = req.query.page;
    const size = req.query.pageSize;

    try {
        const result = await Tutorial.find().skip((page-1)*size).limit(size);
        res.status(200).send(result);
    } catch (error) {
        res.send({ message: error.message });
    }
}

export const getTutorialsByTutor = async (req, res) => {
    const tutor = req.params.tutor;
    console.log(tutor);
    try {
        const result = await Tutorial.find({ "creator": tutor });
        res.status(200).send(result);
    } catch (error) {
        res.status(404).send({ message: error.message });
    }
}

export const getTutorialsBySubscriber = async (req, res) => {
    const subscriber = req.params.subscriber;
    console.log(subscriber);
    try {
        const result = await Tutorial.find({ "subscribers": subscriber });
        res.status(200).send(result);
    } catch (error) {
        res.status(404).send({ message: error.message });
    }
}

export const getTutorialData = async (req, res) => {
    try {
        const total = await Tutorial.count();
        const noSub = await Tutorial.count({ subscribers: [] })
        res.status(200).send({ 
            'totalTutorial': total,
            'noSub': noSub,
            'sub': total-noSub 
        });
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
}

const getDayMonthYear = (date) => {
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    return year + '-0' + month + '-' + day ;
}

export const getTutorialPastDays = async (req, res) => {
    const DAY = parseInt(req.params.days);
    try {
        const pastDays = [...Array(DAY).keys()].map(index => {
            const date = new Date();
            date.setDate(date.getDate() - index)

            return { "date": date, "total": 0, "string": getDayMonthYear(date) };
        });
        
        Tutorial.aggregate(
            [
                {
                    $match: { 'createdAt': { $gte: pastDays[DAY-1].date, $lte: pastDays[0].date } }
                },
                {
                    $group: {
                        _id: { 
                            $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } 
                        },
                        totalTutorial: {
                            $sum: 1
                        }
                    }
                }
            ],
        
            function(err, data) {
              if (err) {
                res.status(400).send(err);
              } else {
                data.forEach(element => {
                    for (let i=0; i<pastDays.length; i++) {
                        if (pastDays[i].string === element._id) {
                            pastDays[i].total = element.totalTutorial;
                            break;
                        }
                    }
                });
                
                res.status(200).json(pastDays.reverse());
              }
            }
        );
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
}