// const path = require('path');
import User from '../models/user.model';
import Role from '../models/role.model';

const getAllUsers = async (req, res) => {
    try {
        const users = await User.find();
        res.status(200).send(users);
        console.log(res.status)
    } catch (error) {
        res.status(404).send({ message: error.message });
    }
}

const createUser = async (req, res) => {
    const user = new User(req.body);

    try {
        console.log(user);
        await user.save();
        res.status(201).json(user);

    } catch (error) {
        res.status(409).send({ message: error.message });
    }
}

const deleteUser = async (req, res) => {
    const firstname = req.params.firstname;

    console.log(firstname)

    try {
        await User.findOneAndDelete(firstname);
        res.send({ message: 'Deleted successfully!' });
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
}

const updateUser = async (req, res) => {
    const userID = req.params.id;
    const user = req.body;
    
    try {
        const prev = await User.findById(userID);
        // console.log('User: '+ prev);
        if (!prev) {
            res.status(404).send('User is not existed!');
        }
        else {
            await User.findByIdAndUpdate(userID, user);
            res.status(200).send({ message: 'Updated successfully!' });
        }
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
}

const getUserByName = async (req, res) => {
    const fname = req.params.fname;
    console.log(fname);
    try {
        const user = await User.findOne({ "username": fname });
        if (user) {
            res.status(202).send(user);
        }
        else {
            res.status(404).send('User is not existed!');
        }
    } catch (error) {
        res.status(404).send({ message: error.message });
    }
}

const getNumberOfUsers = async (req, res) => {
    try {
        const sum = await User.count({});
        res.status(200).send({ 'Number of users': sum });
        // res.status(200).json(sum);
    } catch (error) {
        res.status(404).send({ message: error.message });
    }
}

const getUserData = async (req, res) => {
    try {
        const tutor = await Role.findOne({ name: "moderator" });
        const totalTutor = await User.count({ roles: tutor._id });
        const total = await User.count();
        res.status(200).send({ 
            totalUser:  total - 1,
            totalTutor:  totalTutor,
            totalStudent: total - 1 - totalTutor
        });
    } catch (error) {
        res.status(404).send({ message: error.message });
    }
}

const userBoard = (req, res) => {
    res.status(200).send("User Content.");
};
const adminBoard = (req, res) => {
    res.status(200).send("Admin Content.");
};
const moderatorBoard = (req, res) => {
    res.status(200).send("Moderator Content.");
};

module.exports = {
    //the first key home is a function
    home: (req, res) => {
        res.send('This is home page');
    },
    second: (req, res) => {
        res.send('This is second page');
    },
    getAllUsers,
    createUser,
    deleteUser,
    updateUser,
    getUserByName,
    userBoard,
    adminBoard,
    moderatorBoard,
    getNumberOfUsers,
    getUserData
}


