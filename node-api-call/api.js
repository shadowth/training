// const API = require('./astrology')

// const asyncApiCall = async () => {
//     try {
//         const response = await API.getCompatibility()

//         console.log(response)
//         // console.log(response.data.data.Compatibility)
//     } catch (error) {
//         console.log(error)
//     }
// }


// Doing the import using .default will allow axios.<method> 
// to provide autocomplete and parameter typings
const axios = require('axios').default;

const URL = "https://reqres.in/api/users"

const createUser = async (fname, lname, email) => {
    try {
      const response = await axios.post(URL, {
        first_name: fname,
        last_name: lname,
        email: email
      });

      console.log(response.data);

    } catch (error) {
      console.error('error: ' + error);
    }
}

const getAllUserByPage = async (page) => {
  try {
    const res = await axios.get(URL + '?page=' + page);
    // console.log(res.data.data)
    return res.data.data;

  } catch (error) {
    console.log('error: '+ error);
  }
}

const getUserById = async (id) => {
  try {
    const res = await axios.get(URL + '/' + id)
    
    data = res.data.data
    console.log(data);

    return data.first_name

  } catch (error) {
    console.log('error: ' + error);
  }
}

const updateUserById = async (id, data) => {
  try {
    const res = await axios.put(URL + 'id', data);

    console.log(res.data)
  } catch (error) {
    console.log('error: ' + error);
  }
}

const deleteUserById = async (id) => {
  try {
    const res = await axios.delete(URL + '/' + id);
    console.log(res.status);

  } catch (error) {
    console.log('error: ' + error);
  }
}

module.exports = {
  createUser,
  getAllUserByPage,
  getUserById,
  updateUserById,
  deleteUserById
}


// createUser('lISA', 'tREN', 'ABC');
// getUserById('Janet')
// getAllUserByPage(1)
// updateUserById(4, {first_name: 'Lambo', last_name: 'Ghoinoi'});
// deleteUserById(3);

