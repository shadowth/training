const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const app = express();
//Tell the server file about the .env file
require('dotenv').config();

const port = 8000;

const db = process.env.MONGO_URI ;

import Role from './models/role.model';

app.use(cors())
// app.use(express.json());
app.use(bodyParser.json()); //Use this to recieve req.body //before or after urlencoded: both are ok
// Add bodyParser middleware to the application
app.use(bodyParser.urlencoded({ extended: true }));


// //Express will serve index.html automatically
// app.use(express.static("public"));

//Specify the url prefix and import routes
app.use('/', require('./routes/user.route'));
app.use('/tutorials', require('./routes/tutorial.route'));
app.use('/auth', require('./routes/auth.routes'));
// require('./routes/auth.routes')(app);

// app.listen(port, () => {
//     console.log(`Application is running on port ${port}.`);
// });

// Connect the Express application to MongoDB
mongoose.connect(db, { useNewUrlParser: true })
    .then(() => {
        app.listen(port, () => {
            console.log(`Server is running on port ${port}.`);
            initial();
        });
    })
    .catch(error => console.log(error.message));

function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
        console.log("added 'user' to roles collection");
      });
      new Role({
        name: "moderator"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
        console.log("added 'moderator' to roles collection");
      });
      new Role({
        name: "admin"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
        console.log("added 'admin' to roles collection");
      });
    }
  });
}