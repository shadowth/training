// Using Fetch API
fetch('data.json')
    .then(function(response) {
        console.log('have response');
        return response.json();
    })
    .then(function(data) {
        appendData(data);
    })
    .catch(function (err) {
        console.log('error: ' + err);
    });

function appendData(data) {
    // Need to use table element for insertRow and insertCell
    var table = document.getElementById('table5');
    
    for (let i = 0; i < data.length; i++) {
        var row = table.insertRow(-1);

        // Object to array
        var array = Object.values(data[i]);
        console.log(data[i])

        for (var j=0; j < 3; j++) {
            var cell = row.insertCell(-1);

            if (j===2) {
                var img = document.createElement('img');
                img.src = 'images/' + array[j] + '.jpg';
                cell.appendChild(img);
            }
            else {
                cell.innerHTML = array[j];
            }
        }    
    }

}