export class Tutorial {
    _id?: any;
    title?: string;
    description?: string;
    creator?: string;
    subscribers?: string[];
    published?: boolean;
    updatedAt?: string;
    createdAt?: string;
}
