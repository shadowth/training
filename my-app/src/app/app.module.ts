import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParentComponent } from 'src/app/parent.component';
import { HeroBirthdayComponent } from './child.component';
import { ExponentialStrengthPipe } from './custom-pipe/exponential-strength.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SvgComponent } from './svg/svg.component';
import { HighlightDirective } from './custom-directive/highlight.directive';
import { UnlessDirective } from './custom-directive/unless.directive';
import { AddTutorialComponent } from './components/add-tutorial/add-tutorial.component';
import { TutorialDetailsComponent } from './components/tutorial-details/tutorial-details.component';
import { TutorialListComponent } from './components/tutorial-list/tutorial-list.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ProfileEditorComponent } from './components/profile-editor/profile-editor.component';
import { HttpClientModule } from  '@angular/common/http';
import { DataComponent } from './data/data.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SearchResultComponent } from './components/search-result/search-result.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { ProfileComponent } from './components/profile/profile.component';
import { BoardAdminComponent } from './components/board-admin/board-admin.component';
import { BoardModeratorComponent } from './components/board-moderator/board-moderator.component';
import { BoardUserComponent } from './components/board-user/board-user.component';
import { TableDetailComponent } from './components/table-detail/table-detail.component';
import { ChartsModule } from "ng2-charts";
import { DoughnutChartComponent } from './components/doughnut-chart/doughnut-chart.component';
import { LineChartComponent } from './components/line-chart/line-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    HeroBirthdayComponent,
    ExponentialStrengthPipe,
    SvgComponent,
    HighlightDirective,
    UnlessDirective,
    AddTutorialComponent,
    TutorialDetailsComponent,
    TutorialListComponent,
    PageNotFoundComponent,
    ProfileEditorComponent,
    DataComponent,
    NavbarComponent,
    SearchResultComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardModeratorComponent,
    BoardUserComponent,
    TableDetailComponent,
    DoughnutChartComponent,
    LineChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
    ChartsModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
