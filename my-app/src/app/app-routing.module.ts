import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TutorialDetailsComponent } from 'src/app/components/tutorial-details/tutorial-details.component';
import { AddTutorialComponent } from 'src/app/components/add-tutorial/add-tutorial.component';
import { TutorialListComponent } from 'src/app/components/tutorial-list/tutorial-list.component';
import { PageNotFoundComponent } from 'src/app/components/page-not-found/page-not-found.component';
import { SearchResultComponent } from 'src/app/components/search-result/search-result.component';
import { LoginComponent } from 'src/app/login/login.component';
import { RegisterComponent } from 'src/app/register/register.component';
import { ProfileComponent } from 'src/app/components/profile/profile.component';
import { BoardUserComponent } from 'src/app/components/board-user/board-user.component';
import { BoardAdminComponent } from 'src/app/components/board-admin/board-admin.component';
import { BoardModeratorComponent } from 'src/app/components/board-moderator/board-moderator.component';


const routes: Routes = [
  { path: 'subscriber', component: BoardUserComponent },
  { path: 'admin', component: BoardAdminComponent },
  { path: 'tutor', component: BoardModeratorComponent },
  { path: 'tutorial-list', component: TutorialListComponent },
  { path: 'tutorial-list/:title', component: TutorialDetailsComponent },
  // { path: 'add-tutorial', component: AddTutorialComponent },
  { path: 'signin', component: LoginComponent },
  { path: 'signup', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: '',   redirectTo: '/tutorial-list', pathMatch: 'full' }, // redirect to `tutorial-list`
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
