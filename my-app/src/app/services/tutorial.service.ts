import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Tutorial } from 'src/app/models/tutorial.model';

const baseUrl = 'http://localhost:8000/tutorials'

@Injectable({
  providedIn: 'root'
})
export class TutorialService {

  constructor(private http: HttpClient) { }

  getAllTutorials(): Observable<Tutorial[]> {
    return this.http.get<Tutorial[]>(baseUrl);
  }

  getAll(params: any): Observable<any> {
    const queryParams = new HttpParams()
      .set("page", params.page)
      .set("pageSize", params.pageSize);
    return this.http.get<Tutorial[]>(`${baseUrl}/page`, { params: queryParams });
  }

  createTutorial(data: any): Observable<any> {
    return this.http.post<Tutorial>(baseUrl, data);
  }
  
  getTutorialDetail(title: String): Observable<Tutorial> {
    return this.http.get<Tutorial>(`${baseUrl}/detail/${title}`);
  }

  deleteTutorial(title: any): Observable<Tutorial> {
    return this.http.delete(`${baseUrl}/deleteTut/${title}`)
  }

  updateTutorial(tutorial: Tutorial): Observable<Tutorial> {
    return this.http.put<Tutorial>(`${baseUrl}/updateTut/${tutorial._id}`, tutorial);
  }

  getSearchResult(key: string): Observable<Tutorial[]> {
    const queryParams = new HttpParams().append("key", key)
    return this.http.get<Tutorial[]>(`${baseUrl}/search`, { params: queryParams });
  }
}
