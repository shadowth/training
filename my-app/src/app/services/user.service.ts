import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Tutorial } from 'src/app/models/tutorial.model';
const API_URL = 'http://localhost:8000/';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  // getPublicContent(): Observable<any> {
  //   return this.http.get(API_URL + 'all', { responseType: 'text' });
  // }
  getSubscriberBoard(subscriber: string): Observable<Tutorial[]> {
    return this.http.get<Tutorial[]>(API_URL + 'tutorials/subscriber-course/' + subscriber);
  }
  getModeratorBoard(tutor: string): Observable<Tutorial[]> {
    return this.http.get<Tutorial[]>(API_URL + 'tutorials/tutor-course/' + tutor);
  }
  // getAdminBoard(): Observable<any> {
  //   return this.http.get(API_URL + 'admin', { responseType: 'text' });
  // }

  getTutorialData(): Observable<any> {
    return this.http.get<any>(`${API_URL}tutorials/admin/tutorialData`);
  }

  getUserData(): Observable<any> {
    return this.http.get<any>(`${API_URL}tutorials/admin/userData`);
  }

  getTutorialPastDays(days: number): Observable<any> {
    return this.http.get<any>(`${API_URL}tutorials/admin/tutorialPastDays/` + days);
  }
}
