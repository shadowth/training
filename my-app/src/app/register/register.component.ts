import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../login/login.component.css']
})
export class RegisterComponent implements OnInit {
  signupForm = this.fb.group({
    username: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(5)]],
    role: ['', Validators.required],
  });
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  roleInvalid = false;
  
  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  get username() {
    return this.signupForm.get("username");
  }

  get password() {
    return this.signupForm.get("password");
  }
  
  get email() {
    return this.signupForm.get("email");
  }

  get role() {
    return this.signupForm.get("role");
  }

  onSubmit(): void {
    const username = this.signupForm.value.username;
    const email = this.signupForm.value.email;
    const password = this.signupForm.value.password;
    const role = this.signupForm.value.role;
    if (this.signupForm.get("role")?.invalid) {
      this.roleInvalid = true;
      return;
    }
    this.authService.register(username, email, password, role)
      .subscribe(
        data => {
          console.log(data);
          this.isSignUpFailed = false;
          this.isSuccessful = true;
          this.router.navigate(['/signin'])
        },
        error => {
          this.errorMessage = error.error.message;
          console.log(this.errorMessage);
          this.isSignUpFailed = true;
        }
      );
  }

}
