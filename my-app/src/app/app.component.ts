import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
  // selector: 'app-root',
  // templateUrl: './first-component/first-component.component.html'
})
export class AppComponent implements OnInit {
  private roles: string[] = [];
  isLoggedIn = false;
  username?: string;
  showAdminBoard = false;
  showModeratorBoard = false;
  showSubscriberBoard = false;

  constructor(
    private tokenStorageService: TokenStorageService, 
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;
      this.username = user.username;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showSubscriberBoard = this.roles.includes('ROLE_USER');
    }
  }

  logout(): void {
    this.tokenStorageService.signOut();
    this.router.navigate(["/tutorial-list"]).then(
      () => {
        window.location.reload();
      }
    );
  }
}
