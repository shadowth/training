import { Component } from "@angular/core";

//Parent component practice

@Component({
    selector: 'parent-component',
    template: `
        <p>{{count}}</p>
        <button type="button" (click)="onClicked()">Click</button>
        <button type="button" (click)="reset()">Reset</button>
    `
})

export class ParentComponent {
    count = 0;

    onClicked() {
        this.count++;
    }

    reset() {
        this.count = 0;
    }
}