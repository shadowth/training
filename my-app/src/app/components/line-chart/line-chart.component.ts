import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit, OnChanges {
  @Input() inputDays = 0;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.retrieveData(this.inputDays);
  }

  allChangeLogs: string[] = [];

  ngOnChanges(changes: SimpleChanges) : void {
    for (let propName in changes) {  
      let change = changes[propName];
      
      let curVal  = JSON.stringify(change.currentValue);
      let prevVal = JSON.stringify(change.previousValue);
      let changeLog = `${propName}: currentValue = ${curVal}, previousValue = ${prevVal}`;
      
      
      this.allChangeLogs.push(changeLog);
      
    }
    this.retrieveData(this.inputDays);
    
  }

  retrieveData(days: any): void {
    this.userService.getTutorialPastDays(days).subscribe(
      data => {
        this.chartLabels = data.map((element: any) => {
          return element.string;
        });
        const array = data.map((element: any) => {
          return element.total;
        });
        this.chartData = [
          { data: array, label: 'Tutorial' },
        ];
      }
    )
  }  

  chartData: ChartDataSets[] = [
    { data: [], label: 'Tutorial' },
  ];

  chartLabels: Label[] = []; //['1', '2', '3', '4', '5', '6', '7'];

  
  chartOptions: ChartOptions = {
    responsive: true
  };

  chartColors: Color[] = [
    {
      backgroundColor: 'hsl(193, 41%, 73%, 0.2)',
      borderColor: 'hsl(189, 100%, 50%)'
    }
  ];

  chartLegend = true;
  chartType: ChartType = 'line';
  chartPlugins = [];
}
