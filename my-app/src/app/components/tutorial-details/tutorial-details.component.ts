import { Component, OnInit } from '@angular/core';
import { TutorialService } from 'src/app/services/tutorial.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Tutorial } from 'src/app/models/tutorial.model';
import { FormBuilder } from '@angular/forms';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-tutorial-details',
  templateUrl: './tutorial-details.component.html',
  styleUrls: ['./tutorial-details.component.css', '../tutorial-list/tutorial-list.component.css']
})
export class TutorialDetailsComponent implements OnInit {
  title: any = '';
  tutorial?: Tutorial | any;
  currentUser?: any;
  isSubscriber: any;
  isTutor: any;
  isRoleTutor: any;
  hasSubscribed: any;
  displayForm = "none";

  constructor(
    private tutorialService: TutorialService, 
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private tokenStorage: TokenStorageService
  ) { }

  updateForm = this.fb.group({
    title: [''],
    description: [''],
  }) ;

  ngOnInit(): void {
    this.currentUser = this.tokenStorage.getUser();
    const routeParams = this.route.snapshot.paramMap;
    this.title = routeParams.get('title');
    this.getData(this.title);
    // Check if object is empty
    if (Object.entries(this.currentUser).length > 0) { //not find but stil work
      this.isSubscriber = !!(this.currentUser.roles[0] == "ROLE_USER");
      this.isRoleTutor = !!(this.currentUser.roles[0] == "ROLE_MODERATOR");
    }
  }

  getData(title: any) {
    this.tutorialService.getTutorialDetail(title)
      .subscribe(
        data => {
          this.tutorial = data;
          // console.log(this.tutorial);
          this.hasSubscribed = !!(this.tutorial.subscribers.includes(this.currentUser.username));
          this.updateForm = this.fb.group({
            title: [this.tutorial.title],
            description: [this.tutorial.description],
          }) ;
          this.isTutor = !!(this.currentUser.username == this.tutorial.creator);
        },
        error => {
          console.log(error);
        }
      )
  }

  updateTutorial() {
    this.tutorial.title = this.updateForm.value.title;
    this.tutorial.description = this.updateForm.value.description;
    this.tutorialService.updateTutorial(this.tutorial)
      .subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/tutor']);
        },
        error => {
          console.log(error);
        }
      )
  }

  subscribeTutorial() {
    console.log(this.currentUser.username);
    this.tutorial.subscribers.push(this.currentUser.username);
    console.log(this.tutorial);
    this.tutorialService.updateTutorial(this.tutorial)
      .subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/subscriber']);
        },
        error => {
          console.log(error);
        }
      )
  }

  unsubscribeTutorial() {
    this.tutorial.subscribers.pop(this.currentUser.username);
    this.tutorialService.updateTutorial(this.tutorial)
      .subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/subscriber']);
        },
        error => {
          console.log(error);
        }
      )
  }

  updateEvent() {
    this.displayForm = "flex";
  }

  closeForm() {
    this.displayForm = "none";
  }

}
