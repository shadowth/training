import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Tutorial } from 'src/app/models/tutorial.model';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { TutorialService } from 'src/app/services/tutorial.service';

@Component({
  selector: 'app-board-moderator',
  templateUrl: './board-moderator.component.html',
  styleUrls: ['./board-moderator.component.css', '../tutorial-list/tutorial-list.component.css']
})
export class BoardModeratorComponent implements OnInit {
  tutorials: Tutorial[] = [];
  errorMessage?: string;
  currentUser: any;
  currentIndex = -1;
  page = 1;
  count = 0;
  pageSize = 5;
  isAddTutorial = 'none';

  constructor(
    private userService: UserService, 
    private tokenStorage: TokenStorageService,
    private tutorialService: TutorialService
  ) { }

  ngOnInit(): void {
    this.currentUser = this.tokenStorage.getUser();
    const name = this.currentUser.username;
    this.getContent(name);
  };

  getContent(name: string): void {
    this.userService.getModeratorBoard(name).subscribe(
      data => {
        this.tutorials = data;
        console.log(this.tutorials);
      },
      err => {
        this.errorMessage = JSON.parse(err.error).message;
      }
    )
  }

  updateList(): void {
    this.isAddTutorial = "none";
    this.getContent(this.currentUser.username);
  }

  handlePageChange(event: number) {
    this.page = event;
    this.getContent(this.currentUser.username);
  }

  delete(title: any): void {
    this.tutorialService.deleteTutorial(title)
      .subscribe(
        data => {
          console.log(data);
          this.getContent(this.currentUser.username);
        },
        error => {
          console.log(error);
        }
      )
  }

  addTutorial(): void {
    this.isAddTutorial = "flex";
  }

  close(): void {
    this.isAddTutorial = "none";
  }

}
