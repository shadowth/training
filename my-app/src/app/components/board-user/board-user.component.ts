import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Tutorial } from 'src/app/models/tutorial.model';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { TutorialService } from 'src/app/services/tutorial.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css', '../tutorial-list/tutorial-list.component.css']
})
export class BoardUserComponent implements OnInit {
  tutorials: Tutorial[] = [];
  errorMessage?: string;
  currentUser: any;
  currentIndex = -1;
  page = 1;
  count = 0;
  pageSize = 3;

  constructor(
    private userService: UserService,
    private tokenStorage: TokenStorageService,
    private tutorialService: TutorialService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.currentUser = this.tokenStorage.getUser();
    const name = this.currentUser.username;
    this.getContent(name);
  }

  getContent(name: string): void {
    this.userService.getSubscriberBoard(name).subscribe(
      data => {
        this.tutorials = data;
        console.log(this.tutorials);
      },
      err => {
        this.tutorials = JSON.parse(err.error).message;
      }
    )
  }

  handlePageChange(event: number) {
    this.page = event;
    this.getContent(this.currentUser.username);
  }

  unsubscribeTutorial(tutorial: Tutorial) {
    console.log(tutorial);
    const name = this.currentUser.username;
    if (tutorial.subscribers) {
      console.log(tutorial.subscribers)
      tutorial.subscribers = tutorial.subscribers.filter((subscriber: any) => subscriber !== name);
    }
    this.tutorialService.updateTutorial(tutorial)
      .subscribe(
        data => {
          console.log(data);
          this.getContent(name);
        },
        error => {
          console.log(error);
        }
      )
  }

}
