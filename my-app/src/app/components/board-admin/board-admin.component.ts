import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ChartType } from 'chart.js';
import { Label, MultiDataSet, Color } from 'ng2-charts';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css']
})
export class BoardAdminComponent implements OnInit {
  tutorialData: MultiDataSet = [[0, 0]];
  totalTutorial: any;
  totalUser: any;
  userData: MultiDataSet = [[0, 0]];
  numberOfDays = 7;
  isButton7Active = true;
  isButton10Active = false;
  constructor(private userService: UserService) {
    
  }

  ngOnInit(): void {
    this.userService.getTutorialData().subscribe(
      data => {
        this.tutorialData = [
          [data.sub, data.noSub]
        ];
        this.totalTutorial = data.totalTutorial;
        console.log(this.tutorialData);
      },
      err => {
        this.tutorialData = JSON.parse(err.error).message;
      }
    );
    this.userService.getUserData().subscribe(
      data => {
        console.log(data);
        this.userData = [[data.totalStudent, data.totalTutor]]
        this.totalUser = data.totalUser;
      },
      err => {
        this.tutorialData = JSON.parse(err.error).message;
      }
    )
  }

  input7Days(): void {
    this.numberOfDays = 7;
    this.isButton7Active = true;
    this.isButton10Active = false;
  }

  input10Days(): void {
    this.numberOfDays = 10;
    this.isButton7Active = false;
    this.isButton10Active = true;
  }
}
