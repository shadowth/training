import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TutorialService } from 'src/app/services/tutorial.service';
import { Router } from '@angular/router';
import { Tutorial } from 'src/app/models/tutorial.model';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-add-tutorial',
  templateUrl: './add-tutorial.component.html',
  styleUrls: ['./add-tutorial.component.css']
})
export class AddTutorialComponent implements OnInit {
  @Output() closeEvent = new EventEmitter<any>();
  @Output() afterSaveEvent = new EventEmitter<any>();

  addTutorialForm = this.fb.group({
    title: ['', [Validators.required, Validators.minLength(3)]],
    description: ['', Validators.required],
  });

  currentUser: any;

  closeForm() {
    this.closeEvent.emit();
  }

  constructor(
    private fb: FormBuilder, 
    private tutorialService: TutorialService, 
    private router: Router,
    private token: TokenStorageService
  ) { }

  ngOnInit(): void {

  }
  
  saveTutorial(): void {
    const data: Tutorial = this.addTutorialForm.value;
    this.currentUser = this.token.getUser();
    data.creator = this.currentUser.username;
    this.tutorialService.createTutorial(data)
      .subscribe(
        response => {
          //call parent component
          this.afterSaveEvent.emit();
        },
        error => {
          console.log(error);
        }
      )
  }

  get title() {
    return this.addTutorialForm.get('title');
  }

  get description() {
    return this.addTutorialForm.get('description');
  }

}
