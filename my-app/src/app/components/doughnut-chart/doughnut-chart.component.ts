import { Component, OnInit, Input } from '@angular/core';
import { ChartType } from 'chart.js';
import { Label, MultiDataSet, Color } from 'ng2-charts';

@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.css']
})
export class DoughnutChartComponent implements OnInit {
  @Input() chartLabels: Label[] = [];
  @Input() chartData: MultiDataSet = [];
  chartType: ChartType = 'doughnut';
  chartLegend = true;

  chartColors = [
    {
      backgroundColor: ['hsl(36, 100%, 50%)', 'hsl(189, 100%, 50%)'],
    },
  ];
  constructor() { }

  ngOnInit(): void {

  }

}
