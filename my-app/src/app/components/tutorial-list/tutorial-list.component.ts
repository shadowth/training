import { Component, OnInit } from '@angular/core';
import { Tutorial } from 'src/app/models/tutorial.model';
import { TutorialService } from 'src/app/services/tutorial.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-tutorial-list',
  templateUrl: './tutorial-list.component.html',
  styleUrls: ['./tutorial-list.component.css']
})
export class TutorialListComponent implements OnInit {
  tutorials: Tutorial[] = [];
  searchKey: string = "";
  searchResult?: Tutorial[];

  currentIndex = -1;
  page = 1;
  count = 0;
  pageSize = 3;

  constructor(private tutorialService: TutorialService, private tokenStorage: TokenStorageService) { }

  ngOnInit(): void {
    // console.log(this.currentUser);
    this.retrieveTutorials();
  }

  retrieveTutorials(): void {
    this.tutorialService.getAllTutorials()
      .subscribe(
        data => {
          this.tutorials = data;
          console.log(data);
        },
        error => {
          console.log(error);
        }
      )
  }

  retrieveTutorialsWithParams(): void {
    const params = { 'page': this.page, 'pageSize': this.pageSize };
    this.tutorialService.getAll(params)
      .subscribe(
        response => {
          this.tutorials = response;
          console.log(response);
        },
        error => {
          console.log(error);
        }
      )
  }

  getSearchData(key: string): void {
    console.log(key);
    this.tutorialService.getSearchResult(key)
      .subscribe(
        data => {
          console.log("Search Result: " + data);
          this.searchResult = data;
        },
        error => {
          console.log(error);
        }
      )
  }

  handlePageChange(event: number) {
    this.page = event;
    this.retrieveTutorials();
  }

}
