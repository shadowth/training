import { Component } from "@angular/core";


//Child Component practice
@Component({
    selector: 'app-hero-birthday',
    template: `
        <p>The hero's birthday is {{ birthday | date }}</p>
        <p>The hero's birthday is {{ 50 | currency:'USD':'Dollars' }}</p>
        <p>The hero's birthday is {{ birthday | date:format | uppercase }}</p>
        <button type='button' (click)="toggleFormat()">Toggle Format</button>
        <p>Super power boost: {{ 2 | exponentialStrength: 12 }}</p>
        <br>
        <br>
        <br>
        <h2>Power Boost Calculator</h2>
        <label for="power-input">Normal power: </label>
        <input id="power-input" type="text" [(ngModel)]="power">
        <label for="boost-input">Boost factor: </label>
        <input id="boost-input" type="text" [(ngModel)]="factor">
        <p>
            Super Hero Power: {{power | exponentialStrength: factor}}
        </p>
        <br>
        <button type='button' [disabled]='isUnchanged'>Disabled</button>
    `,
    styles: ['input {margin: .5rm 0;}']
})

export class HeroBirthdayComponent {
    birthday = new Date(1988, 3, 15);
    amout = 50;

    power = 5;
    factor = 1;

    isUnchanged = false;

    toggle = true;

    get format() {
        return this.toggle ? 'shortDate' : 'fullDate'; 
    }

    toggleFormat() {
        this.toggle = !this.toggle;
    }
}